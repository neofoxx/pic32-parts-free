#ifndef UARTDRV_H_8cb7a1b9feb64693bd2da9d5607378b6
#define UARTDRV_H_8cb7a1b9feb64693bd2da9d5607378b6

#include <inttypes.h>

void UARTDrv_Init(uint32_t baud);
void UARTDrv_SendBlocking(uint8_t * buffer, uint32_t length);
uint32_t UARTDrv_GetCount();

#endif
