#include <p32xxxx.h> 
#include <const.h>  
#include <inttypes.h>
#include <BTNDrv.h>
#include <GPIODrv.h>



void BTNDrv_Init(){
	BTN_TRISbits.BTN_TRISPIN = 1;	// Set pin to INPUT
	BTN_ANSELbits.BTN_ANSELBIT = 0;	// Disable analog functionality on the button
	// Button has external pull-up. Idle HIGH
}

uint8_t BTNDrv_GetButtonState(void){
	if (BTN_PORTbits.BTN_PORTPIN == 0){
		return 1;
	}
	else{
		return 0;
	}
}
