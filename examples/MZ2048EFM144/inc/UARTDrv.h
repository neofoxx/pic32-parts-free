#ifndef UARTDRV_H_f082a4c710d7493ebbab54448ac52159
#define UARTDRV_H_f082a4c710d7493ebbab54448ac52159

#include <p32xxxx.h>
#include <const.h>  
#include <inttypes.h>

void UARTDrv_Init(uint32_t pbclk2, uint32_t baud);
void UARTDrv_SendBlocking(uint8_t * buffer, uint32_t length);
uint32_t UARTDrv_GetCount();

#endif
