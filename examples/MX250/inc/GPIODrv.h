#ifndef GPIODRV_H_0501a06b702e4165b5e4599f07fd648f
#define GPIODRV_H_0501a06b702e4165b5e4599f07fd648f


#define UART_TX_ANSELbits	ANSELBbits
#define UART_TX_ANSELBIT	ANSB3
#define UART_TX_TRISbits	TRISBbits
#define UART_TX_TRISPIN		TRISB3
#define UART_TX_LATbits		LATBbits
#define UART_TX_LATPIN		LATB3
#define UART_TX_REMAP		0b0001	// Write to RP***

#define UART_RX_ANSELbits	ANSELBbits
#define UART_RX_ANSELBIT	ANSB13
#define UART_RX_TRISbits	TRISBbits
#define UART_RX_TRISPIN		TRISB13
#define UART_RX_CNPUbits	CNPUBbits
#define UART_RX_CNPUBIT		CNPUB13
#define UART_RX_REMAP		0b0011	// Write to U1RXR

#define LED2_ANSELbits		ANSELBbits
#define LED2_ANSELBIT		ANSB2
#define LED2_TRISbits		TRISBbits
#define LED2_TRISPIN		TRISB2
#define LED2_LATbits		LATBbits
#define LED2_LATPIN			LATB2
#define LED2_LATINV			LATBINV
#define LED2_Mask			(1<<2)

#define BTN2_TRISbits		TRISBbits
#define BTN2_TRISPIN		TRISB4
#define BTN2_PORTbits		PORTBbits
#define BTN2_PORTPIN		RB4


#endif
