pic32-parts-free
================

PIC32 support made entirely out of free and standard parts.

Part support is necessary to complete a toolchain, but the existing solutions required forked GCC versions, and some non-free code as well.

This library allows to build a toolchain using upstream GCC (7.3.0, 8.2.0 tested, 9.2.0), upstream newlib and no non-free code.

The goal of this project is to become a fully functional and documented base for peripheral support libraries.

Supported devices
-----------------

The project is tested on PIC32MX series microcontrollers, the MX440 and MX250. [Future focus](https://gitlab.com/rhn/pic32-parts-free/-/wikis/Supported-devices) is on PIC32MM series.

The [supported boards](https://gitlab.com/rhn/pic32-parts-free/-/wikis/home#supported-hardware) are PIC32-Pinguino-Micro, and ChipKit DP32.

Supported features
------------------

- GPIO
- Interrupts

Toolchain
---------

### Installation

The easiest way to get a working toolchain is to compile things in a container using Podman.

To use Docker instead, replace `podman` with `sudo docker`, and use the `builder` user. Make sure `builder`'s UID/GID can access project files.

To build and create a container called `mipsel_toolchain`, `cd` to the directory where your projects are, and type:

```
podman build tests/gentoo -t gentoo_mipsel
podman run -ti --name mipsel_toolchain -v `pwd`/.:/mnt/pic32:z gentoo_mipsel /bin/bash
```

Note: prepare a few GiBs of space in your home directory to hold the container while it builds. If it fails with insufficient space, [make sure you use fuse-overlayfs](https://github.com/containers/libpod/issues/3846).

Check that your projects directory is correctly visible:

```
$ head -n 1 /mnt/pic32/pic32-parts-free/README.md
pic32-parts-free
```

Check the compiler version with the command:

```
$ mipsel-elf-gcc -v
```

The last line should say something similar to `gcc version 9.2.0 (Gentoo 9.2.0-r3 p4)`.

Type `exit`, and press *Return*. The toolchain has been installed, and your projects directory is visible to it under the path `/mnt/pic32`.

A toolchain set up like this will remember all changes done to it, but only changes in `/mnt/pic32` will be visible outside of the container.

When using Podman, the container will present the *root* user. This is not a real *root*, but an alias of your regular non-container user, and it cannot affect your system as the real *root* would.

### Usage

Start the toolchain container:

```
podman start -ai mipsel_toolchain
```

Hint: to open another shell when you need one, use the following. Don't exit the first one!

```
podman exec -ti mipsel_toolchain /bin/bash
```

### Chipkit toolchain

This library tries to be compatible with the chipkit toolchain, chiefly to compare produced executables.

Usage
-----

### Compile

Each example has a dedicated build script in the `examples` subdirectory. Run the `build_blink_cpu.sh` script using the mipsel toolchain inside the Docker container:

```
cd /mnt/pic32
cd examples/MX440
bash ./build_blink_cpu.sh mipsel
```

### Upload

Uploading files doesn't work inside the Docker container, so make sure the `main32.hex` file is in your `pic32-parts-free` directory somewhere (shown as `/mnt/pic32` inside the container).

Connect the device to the USB port, put it in the boot loader mode, and upload the code using [pic32prog](https://github.com/sergev/pic32prog) **from the host, not inside the container**. Add `sudo` if needed.

```
./pic32prog ./examples/MX440/blink_cpu.hex
```

### Running

Press reset and enjoy the blinking LED.

Supported hardware
------------------

Explicitly supported is the PIC32MX440F256H, found on the Olimex PIC32-Pinguino-Micro board with the old "Pinguino" boot loader.

It should be easy to add more MX series devices, and other Pinguinos. Please let me know if you have tested one.

Credits
-------

- The GNU project for their compilers
- Gentoo for [crossdev](https://wiki.gentoo.org/wiki/Embedded_Handbook/General/Creating_a_cross-compiler) which saved me hours fighting the toolchain
- Pinguino for providing Free [high level code](https://github.com/PinguinoIDE/pinguino-libraries)
- Chipkit contributors for releasing [some of their sources](https://github.com/chipKIT32/pic32-part-support) under a Free license
